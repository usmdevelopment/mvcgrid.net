﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MVCGrid.Attributes
{
    /// <summary>
    /// Excludes property from export functionality.
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public class ExcludeFromExportAttribute : Attribute
    {
        /// <summary>
        /// The name of the grid's column to exclude. Only required if different than the property name itself.
        /// </summary>
        public string ColumnName { get; set; }
    }
}
