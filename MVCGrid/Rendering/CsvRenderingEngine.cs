﻿using MVCGrid.Attributes;
using MVCGrid.Interfaces;
using MVCGrid.Web;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MVCGrid.Rendering
{
    public class CsvRenderingEngine : IMVCGridRenderingEngine
    {
        public bool AllowsPaging
        {
            get { return false; }
        }

        public virtual string GetFilename()
        {
            return "export.csv";
        }

        public virtual void PrepareResponse(System.Web.HttpResponse httpResponse)
        {
            httpResponse.Clear();
            httpResponse.ContentType = "text/csv";
            httpResponse.AddHeader("content-disposition", "attachment; filename=\"" + GetFilename() + "\"");
            httpResponse.BufferOutput = false;
        }

        public void Render(Models.RenderingModel model, Models.GridContext gridContext, TextWriter outputStream)
        {
            if (MVCGridDefinitionTable.GridDictonary.ContainsKey(gridContext.GridName))
            {
                string gridTypeName = MVCGridDefinitionTable.GridDictonary[gridContext.GridName];
                Type gridType = GetType(gridTypeName);
                if (gridType != null)
                {
                    PropertyInfo[] properties = gridType.GetProperties(BindingFlags.Public | BindingFlags.Instance);

                    var excludeAttributeNameInfo = properties
                        .Where(property => property.GetCustomAttributes(typeof(ExcludeFromExportAttribute), true).Length > 0)
                        .Select(property => new
                        {
                            PropertyName = property.Name,
                            PassedAttributeColumnName = 
                                (
                                    (ExcludeFromExportAttribute)property
                                        .GetCustomAttributes(typeof(ExcludeFromExportAttribute), true)
                                        .Single()
                                ).ColumnName
                        });

                    string[] propertyNames = excludeAttributeNameInfo
                        .Select(nameInfo => nameInfo.PassedAttributeColumnName ?? nameInfo.PropertyName)
                        .ToArray();

                    foreach (var propName in propertyNames)
                    {
                        var delete = model.Columns.FirstOrDefault(x => x.Name == propName);
                        if (delete != null)
                        {
                            model.Columns.Remove(delete);
                        }
                    }
                }
            }

            var sw = outputStream;

            StringBuilder sbHeaderRow = new StringBuilder();
            foreach (var col in model.Columns)
            {
                if (sbHeaderRow.Length != 0)
                {
                    sbHeaderRow.Append(",");
                }
                sbHeaderRow.Append(CsvEncode(col.Name));
            }
            sbHeaderRow.AppendLine();
            sw.Write(sbHeaderRow.ToString());

            foreach (var item in model.Rows)
            {
                StringBuilder sbRow = new StringBuilder();
                foreach (var col in model.Columns)
                {
                    var cell = item.Cells[col.Name];

                    if (sbRow.Length != 0)
                    {
                        sbRow.Append(",");
                    }

                    string val = cell.PlainText;

                    sbRow.Append(CsvEncode(val));
                }
                sbRow.AppendLine();
                sw.Write(sbRow.ToString());
            }

        }

        private string CsvEncode(string s)
        {
            if (String.IsNullOrWhiteSpace(s))
            {
                return "\"\"";
            }

            string esc = s.Replace("\"", "\"\"");

            return String.Format("\"{0}\"", esc);
        }


        public void RenderContainer(Models.ContainerRenderingModel model, TextWriter outputStream)
        {
            throw new NotImplementedException("Csv Rendering Engine has no container");
        }

        private static Type GetType(string typeName)
        {
            var type = Type.GetType(typeName);
            if (type != null) return type;
            foreach (var a in AppDomain.CurrentDomain.GetAssemblies())
            {
                type = a.GetType(typeName);
                if (type != null)
                    return type;
            }
            return null;
        }
    }
}
